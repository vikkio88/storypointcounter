﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    static class StoryPoint
    {
        public static string FromTimeSpan(TimeSpan ts)
        {
            string sp = string.Empty;

            if (ts.Hours > 0)
            {
                sp += ts.Hours + "h ";
            }

            if (ts.Minutes > 0)
            {
                sp += ts.Minutes + "m ";
            }

            //if (ts.Seconds > 0)
            //{
            //    sp += ts.Seconds + "s";
            //}

            return sp;
        }
    }
}
