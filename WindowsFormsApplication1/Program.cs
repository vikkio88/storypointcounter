﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace WindowsFormsApplication1
{
    public static class CollectionsExtensions
    {
        public static List<int> FindAllIndex<T>(this List<T> container, Predicate<T> match)
        {
            var items = container.FindAll(match);
            List<int> indexes = new List<int>();
            foreach (var item in items)
            {
                indexes.Add(container.IndexOf(item));
            }

            return indexes;
        }
    }
    public static class Banana
    {
        public const string PATH = @"C:\worklogs\";
        public static void LogEvent(string message)
        {
            try
            {
                createCheckPath();
                DateTime ora = DateTime.Now;
                StreamWriter stream = new StreamWriter(PATH + ora.ToString("dd.MM.yyyy") + ".log.txt", true);
                stream.WriteLine("Log generated: " + DateTime.Now.ToString("dd/MM/yyyy hh.mm.ss") + "\n");
                stream.WriteLine(message);
                stream.WriteLine("");
                stream.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore:" + ex.ToString(), "Errore!");
            }
        }

        public static void createCheckPath(string subpath = null)
        {
            if (subpath == null)
            {
                if (!Directory.Exists(PATH))
                {
                    Directory.CreateDirectory(PATH);
                }
            }
            else
            {
                if (!Directory.Exists(PATH + @"\" + subpath))
                {
                    Directory.CreateDirectory(PATH + @"\" + subpath);
                }
            }
        }

    }

    public class HistoryPoint
    {
        DateTime inizio;
        DateTime fine;

        public HistoryPoint()
        {
            this.inizio = DateTime.Now;
        }

        public HistoryPoint(DateTime inizio)
        {
            this.inizio = inizio;
        }


        public int getHPseconds()
        {
            return this.getHPseconds(DateTime.Now);
        }

        public int getHPseconds(DateTime fine)
        {
            double hp = 0;
            this.fine = DateTime.Now;
            hp = this.getUTS(this.fine) - this.getUTS(this.inizio);
            return Convert.ToInt32(hp);
        }

        public double getHP()
        {
            int secondi = this.getHPseconds();
            double hp = secondi / 3600.0;

            return Math.Round(hp, 2);
        }

        private double getUTS()
        {
            return this.getUTS(DateTime.UtcNow);
        }

        private double getUTS(DateTime data)
        {
            TimeSpan span = (data - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));
            double unixTime = span.TotalSeconds;
            return unixTime;
        }

    }

    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
