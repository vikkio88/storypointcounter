﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml.Linq;
using System.IO;
using System.Threading;

namespace WindowsFormsApplication1
{

    public partial class Form1 : Form
    {
        string url = @"http://";
        Stopwatch orologio = new Stopwatch();
        //DateTime inizio;
        //DateTime fine;
        string descr;
        List<string> _items = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Inserisci descrizione Lavoro prima!");
                textBox1.Focus();
                return;
            }

            descr = textBox1.Text;
            orologio.Reset();
            orologio.Start();
            button2.Enabled = true;
            button1.Enabled = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {

            int hh = orologio.Elapsed.Hours;
            int mm = orologio.Elapsed.Minutes;
            int ss = orologio.Elapsed.Seconds;

            string hhs = stringifyMinute(hh);


            string mms = stringifyMinute(mm);

            string sss = stringifyMinute(ss);

            string elapsed = string.Format("{0}:{1}:{2}", hhs, mms, sss);

            orologio.Stop();
            _items.Add(descr + "  | Date: " + DateTime.Now + " | elapsed: " + elapsed + "  | SP: " + StoryPoint.FromTimeSpan(orologio.Elapsed));
            listBox1.DataSource = null;
            listBox1.DataSource = _items;
            button1.Enabled = true;
            button2.Enabled = false;
            btnLog.Enabled = true;
        }

        private string stringifyMinute(int hh)
        {
            string hhs;
            if (hh < 10)
                hhs = string.Format("0" + hh.ToString());
            else
                hhs = hh.ToString();
            return hhs;
        }

        private string makeStringReport()
        {
            string report = string.Empty;
            //report += "REPORT: --- " + DateTime.Now;
            //report += "\n";
            foreach (string elemento in _items)
            {
                report += elemento + "\n";
            }

            return report;

        }

        private List<string> makeListfromReport(string logfile)
        {
            List<string> list = new List<string>();

            foreach (string line in File.ReadLines(logfile))
            {
                list.Add(line);
            }

            list.RemoveAt(0);
            list.RemoveAt(0);
            list.RemoveAt(list.Count - 1);
            list.RemoveAt(list.Count - 1);
            return list;
        }

        private void LogJira()
        {
            Regex taskmatch = new Regex(@"(.+?)\s|");
            Regex timestringmatch = new Regex(@"\|\sSP:\s(.+?)$");
            string rawstring = listBox1.SelectedItem.ToString();
            string task = taskmatch.Match(rawstring).Groups[1].ToString();
            string timestring = timestringmatch.Match(rawstring).Groups[1].ToString();
            task = task.Trim().ToUpper();
            timestring = timestring.Trim();
            if (timestring.Count() < 1)
            {
                timestring = "0h";
                MessageBox.Show("Do you want to log less than one minute? Really?");
            }
            Clipboard.SetText(timestring);
            string localurl = url + task;
            MessageBox.Show("TimeString copied on clipboard opening jira task: " + task);
            gotoSite(localurl);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string subpath = "config";
            Banana.createCheckPath(subpath);
            try
            {
                var doc = XDocument.Load(Banana.PATH + @"\" + subpath + @"\config.xml");
                var newurl = doc.Elements().Select(x => x.Element("url"));
                url += newurl.First().Value;
            }
            catch (Exception)
            {
                url = @"http://vmprod28.betuniq.eu/jira/browse/";
            }
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            LogJira();
        }

        public void gotoSite(string url)
        {
            System.Diagnostics.Process.Start(url);
        }

        public void textBox1_GotFocus(object sender, EventArgs e)
        {
            textBox1.SelectAll();
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            Banana.LogEvent(makeStringReport());
        }

        private void btnLoadLog_Click(object sender, EventArgs e)
        {
            string fileName = "";
            try
            {

            Stream fileStream = null;
            if ((openFileDialog1.ShowDialog() == DialogResult.OK) && ((fileStream = openFileDialog1.OpenFile()) != null))
            {
                fileName = openFileDialog1.FileName;
            }


                _items = makeListfromReport(fileName);
                listBox1.DataSource = null;
                listBox1.DataSource = _items;
                btnLoadLog.Text = "Load Log";
            }
            catch (Exception)
            {
                btnLoadLog.Text = "Suca!";
            }
        }


    }
}
